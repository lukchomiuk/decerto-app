Building app Maven(v3.5.4): mvn clean package
Running app Maven: mvn spring-boot:run

Building app Gradle(v6.5.1): gradle build
Running app Gradle: gradle run

App can be build using JDK 1.8 or JDK 11

All DEV environment settings stored in /src/resources/application.properties can be override by application.properties outside JAR
