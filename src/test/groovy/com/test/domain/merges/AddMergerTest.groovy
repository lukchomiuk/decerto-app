package com.test.domain.merges

import com.test.domain.providers.Provider
import spock.lang.Specification

class AddMergerTest extends Specification {

    def "test add merger" () {
        given:
        Provider<Integer> p1 = Stub(Provider.class)
        p1.provideNumber() >> 21
        Provider<Integer> p2 = Stub(Provider.class)
        p2.provideNumber() >> 37

        List<Merger> mergers = Arrays.asList(p1, p2)
        AddMerger testedClass = new AddMerger(mergers)

        when:
        def merged = testedClass.retrieveAndMerge()

        then:
        merged == 58
    }

}
