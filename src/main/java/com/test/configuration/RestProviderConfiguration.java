package com.test.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("rest-provider")
@Getter
@Setter
public class RestProviderConfiguration {

    private String apiKey;
    private String quantity;
    private String min;
    private String max;
    private String url;
}
