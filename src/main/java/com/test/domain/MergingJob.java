package com.test.domain;

import com.test.domain.merges.MergeType;
import com.test.domain.merges.Merger;
import com.test.domain.merges.MergerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
class MergingJob {

    private final MergerFactory factory;

    @Scheduled(fixedRateString = "${job.fixedRate}")
    public void execute() {
        Merger merger = factory.getMerger(MergeType.ADD);
        log.info("Merged result : {}", merger.retrieveAndMerge());
    }
}
