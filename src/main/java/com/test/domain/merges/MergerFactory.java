package com.test.domain.merges;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public final class MergerFactory {

    private final List<Merger> mergers;

    public Merger getMerger(final MergeType type) {
        return mergers.stream().filter(supportedType(type)).findFirst().orElseThrow(() -> new IllegalArgumentException("Cannot find merger for type : " + type));
    }

    private Predicate<Merger> supportedType(final MergeType type) {
        return m -> m.isSupportedType(type);
    }

}
