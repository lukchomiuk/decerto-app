package com.test.domain.merges;

import com.test.domain.providers.Provider;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
class AddMerger extends Merger<Integer> {

    AddMerger(List<Provider> providers) {
        super(providers);
    }

    @Override
    protected Integer merge(List<Number> toMerge) {
        return toMerge.stream().map(c -> (Integer) c).collect(Collectors.summingInt(Integer::intValue));
    }

    @Override
    protected MergeType getType() {
        return MergeType.ADD;
    }

}
