package com.test.domain.merges;

import com.test.domain.providers.Provider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
public abstract class Merger<V extends Number> {

    private final List<Provider> providers;

    public V retrieveAndMerge() {
        log.info("Merging type : {}", getType());
        return merge(providers.stream()
                              .map((Function<Provider, Number>) Provider::provideNumber)
                              .collect(Collectors.toList())
        );
    }

    protected abstract V merge(final List<Number> toMerge);

    protected abstract MergeType getType();

    protected boolean isSupportedType(final MergeType type) {
        return getType() == type;
    }

}
