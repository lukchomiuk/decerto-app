package com.test.domain.providers.db;

import com.test.domain.providers.Provider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
class DatabaseProvider implements Provider {

    private final NumberRepository numberRepository;

    public Integer provideNumber() {
        int randomNumberUsingNextInt = getRandomNumberUsingNextInt(Long.valueOf(numberRepository.count()).intValue());
        Integer number = numberRepository.findById(Integer.valueOf(randomNumberUsingNextInt).longValue()).get().getNumber();
        log.info("Number from database {}", number);
        return number;
    }

    private int getRandomNumberUsingNextInt(int max) {
        Random random = new Random();

        return random.nextInt(max - 1) + 1;
    }
}
