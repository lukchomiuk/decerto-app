package com.test.domain.providers.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface NumberRepository extends JpaRepository<Number, Long> {

}
