package com.test.domain.providers.rest.model;

import lombok.Data;

@Data
public class Result {

    private Random random;
}
