package com.test.domain.providers.rest.model;

import lombok.Data;

import java.util.List;

@Data
public class Random {

    private List<Integer> data;
}
