package com.test.domain.providers.rest.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Params {

    private String apiKey;
    private String n;
    private String min;
    private String max;
}
