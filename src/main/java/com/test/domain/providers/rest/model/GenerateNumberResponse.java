package com.test.domain.providers.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenerateNumberResponse {

    private Result result;
    private Error error;

}
