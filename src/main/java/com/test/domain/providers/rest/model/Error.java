package com.test.domain.providers.rest.model;

import lombok.Data;

@Data
public class Error {

    private int code;
    private String message;
}
