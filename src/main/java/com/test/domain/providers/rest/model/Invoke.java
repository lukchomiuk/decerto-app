package com.test.domain.providers.rest.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Invoke {

    private final String method = "generateIntegers";
    private final String jsonrpc = "2.0";
    private final int id = 1;
    private Params params;
}
