package com.test.domain.providers.rest;

import com.test.configuration.RestProviderConfiguration;
import com.test.domain.providers.Provider;
import com.test.domain.providers.rest.model.GenerateNumberResponse;
import com.test.domain.providers.rest.model.Invoke;
import com.test.domain.providers.rest.model.Params;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Slf4j
class RestProvider implements Provider {

    private final RestTemplate restTemplate;
    private final RestProviderConfiguration restProviderConfiguration;

    public Integer provideNumber() {
        HttpEntity<Invoke> request = new HttpEntity<>(prepareRequest());
        GenerateNumberResponse generateNumberResponse = restTemplate.postForObject(restProviderConfiguration.getUrl(), request, GenerateNumberResponse.class);
        if (generateNumberResponse.getError() != null) {
            throw new RuntimeException("Error during communication with REST service, message returned :" + generateNumberResponse.getError().getMessage());
        }
        log.info("Number from random.org : {}", generateNumberResponse.getResult().getRandom().getData());

        return generateNumberResponse.getResult().getRandom().getData().get(0);
    }

    private Invoke prepareRequest() {
        return Invoke.builder()
                .params(
                        Params.builder()
                                .apiKey(restProviderConfiguration.getApiKey())
                                .n(restProviderConfiguration.getQuantity())
                                .min(restProviderConfiguration.getMin())
                                .max(restProviderConfiguration.getMax())
                        .build()
                ).build();
    }

}
