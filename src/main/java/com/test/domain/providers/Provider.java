package com.test.domain.providers;

public interface Provider<T extends Number> {

    T provideNumber();
}
