DROP TABLE IF EXISTS number_storage;

CREATE TABLE number_storage (
  id INT AUTO_INCREMENT PRIMARY KEY,
  number INTEGER NOT NULL
);

INSERT INTO number_storage (number) VALUES
  (3),
  (5),
  (7),
  (3),
  (5),
  (4),
  (8);